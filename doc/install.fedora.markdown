# Paperwork installation on GNU/Linux Fedora


## Package

    $ sudo dnf install paperwork

## Runtime dependencies

You may wish to install additional languages for OCR with tesseract, e.g., for
French:

    $ sudo dnf install tesseract-langpack-fra


## Running Paperwork

A shortcut should be available in the menus of your window manager (you may
have to log out first).

You can also start Paperwork by running the command 'paperwork'.

Enjoy :-)
